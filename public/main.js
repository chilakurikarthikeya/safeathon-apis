(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-header></app-header>\n<router-outlet></router-outlet>\n<app-footer></app-footer>"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'safeathon';
    }
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: getAuthServiceConfigs, AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getAuthServiceConfigs", function() { return getAuthServiceConfigs; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _components_home_home_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./components/home/home.component */ "./src/app/components/home/home.component.ts");
/* harmony import */ var _components_header_header_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./components/header/header.component */ "./src/app/components/header/header.component.ts");
/* harmony import */ var _components_footer_footer_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./components/footer/footer.component */ "./src/app/components/footer/footer.component.ts");
/* harmony import */ var _components_login_login_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./components/login/login.component */ "./src/app/components/login/login.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_auth_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/auth.service */ "./src/app/auth.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _components_register_register_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./components/register/register.component */ "./src/app/components/register/register.component.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _auth_auth_guard__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./auth/auth.guard */ "./src/app/auth/auth.guard.ts");
/* harmony import */ var _auth_auth_interceptor__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./auth/auth.interceptor */ "./src/app/auth/auth.interceptor.ts");
/* harmony import */ var angular_6_social_login__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! angular-6-social-login */ "./node_modules/angular-6-social-login/angular-6-social-login.umd.js");
/* harmony import */ var angular_6_social_login__WEBPACK_IMPORTED_MODULE_17___default = /*#__PURE__*/__webpack_require__.n(angular_6_social_login__WEBPACK_IMPORTED_MODULE_17__);
/* harmony import */ var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @angular/material/form-field */ "./node_modules/@angular/material/esm5/form-field.es5.js");
/* harmony import */ var _angular_material_select__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @angular/material/select */ "./node_modules/@angular/material/esm5/select.es5.js");
/* harmony import */ var _angular_material_input__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! @angular/material/input */ "./node_modules/@angular/material/esm5/input.es5.js");
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! @angular/material/icon */ "./node_modules/@angular/material/esm5/icon.es5.js");
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! @angular/material/button */ "./node_modules/@angular/material/esm5/button.es5.js");
/* harmony import */ var _components_after_login_after_login_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./components/after-login/after-login.component */ "./src/app/components/after-login/after-login.component.ts");
/* harmony import */ var _components_events_events_component__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./components/events/events.component */ "./src/app/components/events/events.component.ts");
/* harmony import */ var _angular_material_grid_list__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! @angular/material/grid-list */ "./node_modules/@angular/material/esm5/grid-list.es5.js");
/* harmony import */ var ng2_file_upload__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ng2-file-upload */ "./node_modules/ng2-file-upload/index.js");
/* harmony import */ var ng2_file_upload__WEBPACK_IMPORTED_MODULE_26___default = /*#__PURE__*/__webpack_require__.n(ng2_file_upload__WEBPACK_IMPORTED_MODULE_26__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


















// social login

// MATERIAL DESIGN








// file upload

// Configs
function getAuthServiceConfigs() {
    var config = new angular_6_social_login__WEBPACK_IMPORTED_MODULE_17__["AuthServiceConfig"]([
        {
            id: angular_6_social_login__WEBPACK_IMPORTED_MODULE_17__["FacebookLoginProvider"].PROVIDER_ID,
            provider: new angular_6_social_login__WEBPACK_IMPORTED_MODULE_17__["FacebookLoginProvider"]('')
        },
        {
            id: angular_6_social_login__WEBPACK_IMPORTED_MODULE_17__["GoogleLoginProvider"].PROVIDER_ID,
            provider: new angular_6_social_login__WEBPACK_IMPORTED_MODULE_17__["GoogleLoginProvider"]('286287682128-6qq382tdfgrhl300bncdbng9hos450sk.apps.googleusercontent.com')
        },
        {
            id: angular_6_social_login__WEBPACK_IMPORTED_MODULE_17__["LinkedinLoginProvider"].PROVIDER_ID,
            provider: new angular_6_social_login__WEBPACK_IMPORTED_MODULE_17__["LinkedinLoginProvider"]('1098828800522-m2ig6bieilc3tpqvmlcpdvrpvn86q4ks.apps.googleusercontent.com')
        },
    ]);
    return config;
}
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_2__["AppComponent"],
                _components_home_home_component__WEBPACK_IMPORTED_MODULE_3__["HomeComponent"],
                _components_header_header_component__WEBPACK_IMPORTED_MODULE_4__["HeaderComponent"],
                _components_footer_footer_component__WEBPACK_IMPORTED_MODULE_5__["FooterComponent"],
                _components_login_login_component__WEBPACK_IMPORTED_MODULE_6__["LoginComponent"],
                _components_register_register_component__WEBPACK_IMPORTED_MODULE_12__["RegisterComponent"],
                _components_after_login_after_login_component__WEBPACK_IMPORTED_MODULE_23__["AfterLoginComponent"],
                _components_events_events_component__WEBPACK_IMPORTED_MODULE_24__["EventsComponent"]
                // MatFormFieldModule
                // AuthService
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_9__["FormsModule"],
                _angular_http__WEBPACK_IMPORTED_MODULE_10__["HttpModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_13__["HttpClientModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_7__["RouterModule"].forRoot([
                    { path: '', redirectTo: '/home', pathMatch: 'full' },
                    { path: 'home', component: _components_home_home_component__WEBPACK_IMPORTED_MODULE_3__["HomeComponent"] },
                    { path: 'login', component: _components_login_login_component__WEBPACK_IMPORTED_MODULE_6__["LoginComponent"] },
                    { path: 'register', component: _components_register_register_component__WEBPACK_IMPORTED_MODULE_12__["RegisterComponent"] },
                    { path: 'after_login', component: _components_after_login_after_login_component__WEBPACK_IMPORTED_MODULE_23__["AfterLoginComponent"], canActivate: [_auth_auth_guard__WEBPACK_IMPORTED_MODULE_15__["AuthGuard"]] },
                    { path: 'events', component: _components_events_events_component__WEBPACK_IMPORTED_MODULE_24__["EventsComponent"], canActivate: [_auth_auth_guard__WEBPACK_IMPORTED_MODULE_15__["AuthGuard"]] }
                ]),
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_11__["BrowserAnimationsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_9__["ReactiveFormsModule"], angular_6_social_login__WEBPACK_IMPORTED_MODULE_17__["SocialLoginModule"], _angular_common__WEBPACK_IMPORTED_MODULE_14__["CommonModule"], ng2_file_upload__WEBPACK_IMPORTED_MODULE_26__["FileUploadModule"],
                _angular_material_form_field__WEBPACK_IMPORTED_MODULE_18__["MatFormFieldModule"], _angular_material_select__WEBPACK_IMPORTED_MODULE_19__["MatSelectModule"], _angular_material_input__WEBPACK_IMPORTED_MODULE_20__["MatInputModule"], _angular_material_icon__WEBPACK_IMPORTED_MODULE_21__["MatIconModule"], _angular_material_button__WEBPACK_IMPORTED_MODULE_22__["MatButtonModule"], _angular_material_grid_list__WEBPACK_IMPORTED_MODULE_25__["MatGridListModule"]
            ],
            providers: [src_app_auth_service__WEBPACK_IMPORTED_MODULE_8__["AuthService1"], _auth_auth_guard__WEBPACK_IMPORTED_MODULE_15__["AuthGuard"],
                {
                    provide: angular_6_social_login__WEBPACK_IMPORTED_MODULE_17__["AuthServiceConfig"],
                    useFactory: getAuthServiceConfigs
                },
                {
                    provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_13__["HTTP_INTERCEPTORS"],
                    useClass: _auth_auth_interceptor__WEBPACK_IMPORTED_MODULE_16__["AuthInterceptor"],
                    multi: true
                }
            ],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_2__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/auth.service.ts":
/*!*********************************!*\
  !*** ./src/app/auth.service.ts ***!
  \*********************************/
/*! exports provided: AuthService1 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService1", function() { return AuthService1; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_add_operator_map__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/add/operator/map */ "./node_modules/rxjs-compat/_esm5/add/operator/map.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var angular2_jwt__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! angular2-jwt */ "./node_modules/angular2-jwt/angular2-jwt.js");
/* harmony import */ var angular2_jwt__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(angular2_jwt__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../environments/environment */ "./src/environments/environment.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


// import {HttpClient} from '@angular/common/http';

// import 'rxjs/add/add/operator/map';

// import { map } from 'rxjs/operators';
// import { map } from 'rxjs';



var AuthService1 = /** @class */ (function () {
    function AuthService1(http, router) {
        this.http = http;
        this.router = router;
        this.noAuthHeader = { headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({ 'NoAuth': 'True' }) };
    }
    AuthService1.prototype.register = function (user) {
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_1__["Headers"]();
        headers.append('Content-Type', 'application/json');
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_6__["environment"].apiBaseUrl + '/register', user, this.noAuthHeader);
    };
    AuthService1.prototype.login = function (user) {
        this.user_email = user['mail'];
        // alert(user['email']);
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_6__["environment"].apiBaseUrl + '/authenticate', user, this.noAuthHeader);
    };
    AuthService1.prototype.getevents = function () {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_6__["environment"].apiBaseUrl + '/user_profile');
    };
    AuthService1.prototype.getUserPfofile = function () {
        console.log('user callrd');
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_6__["environment"].apiBaseUrl + '/user_profile');
    };
    AuthService1.prototype.update = function (method) {
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_1__["Headers"]();
        headers.append('Content-Type', 'application/json');
        return this.http.put(_environments_environment__WEBPACK_IMPORTED_MODULE_6__["environment"].apiBaseUrl + '/update', method);
    };
    AuthService1.prototype.setToken = function (token) {
        localStorage.setItem('token', token);
    };
    AuthService1.prototype.getToken = function () {
        return localStorage.getItem('token');
    };
    AuthService1.prototype.deleteToken = function () {
        localStorage.removeItem('token');
    };
    AuthService1.prototype.getUserPayload = function () {
        var token = this.getToken();
        if (token) {
            var userPayload = atob(token.split('.')[1]);
            console.log(userPayload);
            return JSON.parse(userPayload);
        }
        else {
            return null;
        }
    };
    AuthService1.prototype.isLoggedIn = function () {
        var userPayload = this.getUserPayload();
        if (userPayload) {
            console.log(' IsLOgged()');
            console.log(userPayload.exp > Date.now() / 1000);
            return userPayload.exp > Date.now() / 1000;
        }
        else {
            console.log('no login');
            return false;
        }
    };
    AuthService1.prototype.loggedIn = function () {
        return Object(angular2_jwt__WEBPACK_IMPORTED_MODULE_5__["tokenNotExpired"])();
    };
    AuthService1 = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
    ], AuthService1);
    return AuthService1;
}());



/***/ }),

/***/ "./src/app/auth/auth.guard.ts":
/*!************************************!*\
  !*** ./src/app/auth/auth.guard.ts ***!
  \************************************/
/*! exports provided: AuthGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthGuard", function() { return AuthGuard; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../auth.service */ "./src/app/auth.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AuthGuard = /** @class */ (function () {
    function AuthGuard(authService, router) {
        this.authService = authService;
        this.router = router;
    }
    AuthGuard.prototype.canActivate = function (next, state) {
        if (!this.authService.isLoggedIn()) {
            this.router.navigateByUrl('/home');
            this.authService.deleteToken();
            return false;
        }
        return true;
    };
    AuthGuard = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService1"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], AuthGuard);
    return AuthGuard;
}());



/***/ }),

/***/ "./src/app/auth/auth.interceptor.ts":
/*!******************************************!*\
  !*** ./src/app/auth/auth.interceptor.ts ***!
  \******************************************/
/*! exports provided: AuthInterceptor */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthInterceptor", function() { return AuthInterceptor; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../auth.service */ "./src/app/auth.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AuthInterceptor = /** @class */ (function () {
    function AuthInterceptor(authService, router) {
        this.authService = authService;
        this.router = router;
    }
    AuthInterceptor.prototype.intercept = function (req, next) {
        var _this = this;
        if (req.headers.get('noauth')) {
            return next.handle(req.clone());
        }
        else {
            var clonedreq = req.clone({
                headers: req.headers.set('Authorization', 'Bearer ' + this.authService.getToken())
            });
            return next.handle(clonedreq).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["tap"])(function (event) { }, function (err) {
                if (err.error.auth == false) {
                    _this.router.navigateByUrl('/home');
                }
            }));
        }
    };
    AuthInterceptor = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService1"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], AuthInterceptor);
    return AuthInterceptor;
}());



/***/ }),

/***/ "./src/app/components/after-login/after-login.component.css":
/*!******************************************************************!*\
  !*** ./src/app/components/after-login/after-login.component.css ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/after-login/after-login.component.html":
/*!*******************************************************************!*\
  !*** ./src/app/components/after-login/after-login.component.html ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<form #signInForm=\"ngForm\" (ngSubmit)=\"signInForm.valid && onSubmit(signInForm)\">\n\n    yor mail:<input placeholder=\"Enter your email\" #email=\"ngModel\" [(ngModel)]=\"model.email\" name=\"email\" required [ngClass]=\"{'invalid-textbox' :signInForm.submitted && !email.valid }\">\n\n    <input type=\"submit\" class=\"btn btn-primary\" value=\"submit\">\n\n</form>"

/***/ }),

/***/ "./src/app/components/after-login/after-login.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/components/after-login/after-login.component.ts ***!
  \*****************************************************************/
/*! exports provided: AfterLoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AfterLoginComponent", function() { return AfterLoginComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/auth.service */ "./src/app/auth.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AfterLoginComponent = /** @class */ (function () {
    function AfterLoginComponent(authService, router) {
        this.authService = authService;
        this.router = router;
        this.model = {
            email: '',
        };
    }
    AfterLoginComponent.prototype.ngOnInit = function () {
    };
    AfterLoginComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-after-login',
            template: __webpack_require__(/*! ./after-login.component.html */ "./src/app/components/after-login/after-login.component.html"),
            styles: [__webpack_require__(/*! ./after-login.component.css */ "./src/app/components/after-login/after-login.component.css")]
        }),
        __metadata("design:paramtypes", [src_app_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService1"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], AfterLoginComponent);
    return AfterLoginComponent;
}());



/***/ }),

/***/ "./src/app/components/events/events.component.css":
/*!********************************************************!*\
  !*** ./src/app/components/events/events.component.css ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".eve {\n    width: 50%;\n}"

/***/ }),

/***/ "./src/app/components/events/events.component.html":
/*!*********************************************************!*\
  !*** ./src/app/components/events/events.component.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"eve\">\n    <div class=\"table-responsive\">\n\n        <!--Table-->\n        <table class=\"table align-items-center\">\n\n            <!--Table head-->\n            <thead>\n                <tr>\n                    <th>Event</th>\n                    <th class=\"th-sm\">Reg Status</th>\n                    <th class=\"th-sm\">Payment Staus</th>\n                    <!-- <th class=\"th-sm\">Lorem ipsum dolor</th> -->\n                </tr>\n            </thead>\n            <!--Table head-->\n\n            <!--Table body-->\n            <tbody>\n                <tr>\n                    <th> My Idea</th>\n                    <td *ngIf=\"!UserDetails.events.MyIdea_reg\"><button (click)=\"onupdate(1,1)\" type=\"button\" class=\"btn btn-success\">Register</button></td>\n                    <td *ngIf=\"UserDetails.events.MyIdea_reg\"><button [disabled]=\"true\" type=\"button\" class=\"btn btn-success\">Regisered</button></td>\n                    <td *ngIf=\"!UserDetails.events.MyIdea_payment\"><button mat-stroked-button color='primary'>Make_Payment</button></td>\n                    <td *ngIf=\"UserDetails.events.MyIdea_payment\"><i class=\"material-icons\">done </i></td>\n                    <!-- <td>\n                        <input type=\"file\" (change)=\"onFileSelected($event)\">\n                        <button type=\"button\" (click)=\"onUpload()\">upload</button>\n                    </td> -->\n                </tr>\n                <tr>\n                    <th>Analysis guru</th>\n                    <td *ngIf=\"!UserDetails.events.AnalysisGuru_reg\"><button (click)=\"onupdate(2,1)\" type=\"button\" class=\"btn btn-success\">Register</button></td>\n                    <td *ngIf=\"UserDetails.events.AnalysisGuru_reg\"><button [disabled]=\"true\" type=\"button\" class=\"btn btn-success\">Regisered</button></td>\n                    <td *ngIf=\"!UserDetails.events.AnalysisGuru_payment\"><button mat-stroked-button color='primary'>Make_Payment</button></td>\n                    <td *ngIf=\"UserDetails.events.AnalysisGuru_payment\"><i class=\"material-icons\">done </i></td>\n                    <!-- <td>Lorem ipsum dolor</td> -->\n                </tr>\n                <tr>\n                    <th>Flash Mob</th>\n                    <td *ngIf=\"!UserDetails.events.FlashMob_reg\"><button (click)=\"onupdate(3,1)\" type=\"button\" class=\"btn btn-success\">Register</button></td>\n                    <td *ngIf=\"UserDetails.events.FlashMob_reg\"><button [disabled]=\"true\" type=\"button\" class=\"btn btn-success\">Regisered</button></td>\n                    <td *ngIf=\"!UserDetails.events.FlashMob_payment\"><button mat-stroked-button color='primary'>Make_Payment</button></td>\n                    <td *ngIf=\"UserDetails.events.FlashMob_payment\"><i class=\"material-icons\">done </i></td>\n                    <!-- <td>Lorem ipsum dolor</td> -->\n                </tr>\n                <tr>\n                    <th>Coding challenge</th>\n                    <td *ngIf=\"!UserDetails.events.CoadingChallenge_reg\"><button (click)=\"onupdate(4,1)\" type=\"button\" class=\"btn btn-success\">Register</button></td>\n                    <td *ngIf=\"UserDetails.events.CoadingChallenge_reg\"><button [disabled]=\"true\" type=\"button\" class=\"btn btn-success\">Regisered</button></td>\n                    <td *ngIf=\"!UserDetails.events.CoadingChallenge_payment\">\n                        <button mat-stroked-button color='primary'>Make_Payment</button>\n                    </td>\n                    <td *ngIf=\"UserDetails.events.CoadingChallenge_payment\"><i class=\"material-icons\">done </i></td>\n                    <!-- <td>Lorem ipsum dolor</td> -->\n                </tr>\n                <tr>\n                    <th>Short film</th>\n                    <td *ngIf=\"!UserDetails.events.ShortFilm_reg\"><button (click)=\"onupdate(5,1)\" type=\"button\" class=\"btn btn-success\">Register</button></td>\n                    <td *ngIf=\"UserDetails.events.ShortFilm_reg\"><button [disabled]=\"true\" type=\"button\" class=\"btn btn-success\">Regisered</button></td>\n                    <td *ngIf=\"!UserDetails.events.ShortFilm_payment\"><button mat-stroked-button color='primary'>Make_Payment</button></td>\n                    <td *ngIf=\"UserDetails.events.ShortFilm_payment\"><i class=\"material-icons\">done </i></td>\n                    <!-- <td>Lorem ipsum dolor</td> -->\n                </tr>\n                <tr>\n                    <th>PubG</th>\n                    <td *ngIf=\"!UserDetails.events.PubG_reg\"><button (click)=\"onupdate(6,1)\" type=\"button\" class=\"btn btn-success\">Register</button></td>\n                    <td *ngIf=\"UserDetails.events.PubG_reg\"><button [disabled]=\"true\" type=\"button\" class=\"btn btn-success\">Regisered</button></td>\n                    <td *ngIf=\"!UserDetails.events.PubG_payment\"><button mat-stroked-button color='primary'>Make_Payment</button></td>\n                    <td *ngIf=\"UserDetails.events.PubG_payment\"><i class=\"material-icons\">done </i></td>\n                    <!-- <td>Lorem ipsum dolor</td> -->\n                </tr>\n                <tr>\n                    <th>TikTok</th>\n                    <td *ngIf=\"!UserDetails.events.TikTok_reg\"><button (click)=\"onupdate(7,1)\" type=\"button\" class=\"btn btn-success\">Register</button></td>\n                    <td *ngIf=\"UserDetails.events.TikTok_reg\"><button [disabled]=\"true\" type=\"button\" class=\"btn btn-success\">Regisered</button></td>\n                    <td *ngIf=\"!UserDetails.events.TikTok_payment\"><button mat-stroked-button color='primary'>Make_Payment</button></td>\n                    <td *ngIf=\"UserDetails.events.TikTok_payment\"><i class=\"material-icons\">done </i></td>\n                    <!-- <td>Lorem ipsum dolor</td> -->\n                </tr>\n\n\n            </tbody>\n            <!--Table body-->\n\n        </table>\n        <!--Table-->\n\n    </div>\n</div>\n\n<!-- <div class=\"container\">\n    <button md-raised-button color=\"primary\" (click)=\"fileInput.click()\">Upload</button>\n    <input hidden type=\"file\" #fileInput>\n</div> -->"

/***/ }),

/***/ "./src/app/components/events/events.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/components/events/events.component.ts ***!
  \*******************************************************/
/*! exports provided: EventsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EventsComponent", function() { return EventsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/auth.service */ "./src/app/auth.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var EventsComponent = /** @class */ (function () {
    function EventsComponent(authService, router, http) {
        this.authService = authService;
        this.router = router;
        this.http = http;
        this.selectedFile = null;
    }
    EventsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.authService.getUserPfofile().subscribe(function (res) {
            _this.UserDetails = res['user'];
            console.log(_this.UserDetails);
            console.log('karthik');
            _this.MyIdea = _this.UserDetails.events.MyIdea_reg;
        }, function (err) {
            console.log('NO DATA');
        });
    };
    EventsComponent.prototype.onupdate = function (event, reg) {
        var _this = this;
        this.update = {
            event: event,
            opt: reg
        };
        this.authService.update(this.update).subscribe(function (res) {
            alert('successfully Registred');
            window.location.reload();
            _this.router.navigateByUrl('/events');
        }, function (err) {
            alert('registration failed');
        });
    };
    // upload
    EventsComponent.prototype.onFileSelected = function (event) {
        // console.log(event);
        this.selectedFile = event.target.files[0];
    };
    EventsComponent.prototype.onUpload = function () {
    };
    EventsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-events',
            template: __webpack_require__(/*! ./events.component.html */ "./src/app/components/events/events.component.html"),
            styles: [__webpack_require__(/*! ./events.component.css */ "./src/app/components/events/events.component.css")]
        }),
        __metadata("design:paramtypes", [src_app_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService1"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]])
    ], EventsComponent);
    return EventsComponent;
}());



/***/ }),

/***/ "./src/app/components/footer/footer.component.css":
/*!********************************************************!*\
  !*** ./src/app/components/footer/footer.component.css ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/footer/footer.component.html":
/*!*********************************************************!*\
  !*** ./src/app/components/footer/footer.component.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- start footer Area -->\n<footer class=\"footer-area section-gap\">\n    <div class=\"container\">\n        <div class=\"row\">\n            <div class=\"col-lg-5 col-md-6 col-sm-6\">\n                <div class=\"single-footer-widget\">\n                    <h6>About Us</h6>\n                    <p>\n                        Lets together create an ideal family <br> for every girl out there.\n                    </p>\n                    <p class=\"footer-text\">\n\n                    </p>\n                </div>\n            </div>\n            <div class=\"col-lg-5  col-md-6 col-sm-6\">\n                <div class=\"single-footer-widget\">\n                    <h6>Newsletter</h6>\n                    <p>Stay update with our latest</p>\n                    <div class=\"\" id=\"mc_embed_signup\">\n                        <form target=\"_blank\" novalidate=\"true\" action=\"https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&amp;id=92a4423d01\" method=\"get\" class=\"form-inline\">\n                            <input class=\"form-control\" name=\"EMAIL\" placeholder=\"Enter Email\" onfocus=\"this.placeholder = ''\" onblur=\"this.placeholder = 'Enter Email '\" required=\"\" type=\"email\">\n                            <button class=\"click-btn btn btn-default\"><i class=\"fa fa-long-arrow-right\" aria-hidden=\"true\"></i></button>\n                            <div style=\"position: absolute; left: -5000px;\">\n                                <input name=\"b_36c4fd991d266f23781ded980_aefe40901a\" tabindex=\"-1\" value=\"\" type=\"text\">\n                            </div>\n\n                            <div class=\"info\"></div>\n                        </form>\n                    </div>\n                </div>\n            </div>\n            <div class=\"col-lg-2 col-md-6 col-sm-6 social-widget\">\n                <div class=\"single-footer-widget\">\n                    <h6>Follow Us</h6>\n                    <p>Let us be social</p>\n                    <div class=\"footer-social d-flex align-items-center\">\n                        <a href=\"https://www.facebook.com/lifeofgirl2020\" target=\"/\"><i class=\"fa fa-facebook\"></i></a>\n                        <a href=\"https://www.twitter.com/lifeofgirl\" target=\"/\"><i class=\"fa fa-twitter\"></i></a>\n                        <a href=\"https://www.instagram.com/lifeofgirl\" target=\"/\"><i class=\"fa fa-instagram\"></i></a>\n                        <a href=\"https://www.youtube.com/channel/UCQO9ROQKdTuRVKWqEgaYiYw\" target=\"/\"><i class=\"fa fa-youtube\"></i></a>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</footer>"

/***/ }),

/***/ "./src/app/components/footer/footer.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/components/footer/footer.component.ts ***!
  \*******************************************************/
/*! exports provided: FooterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FooterComponent", function() { return FooterComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var FooterComponent = /** @class */ (function () {
    function FooterComponent() {
    }
    FooterComponent.prototype.ngOnInit = function () {
    };
    FooterComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-footer',
            template: __webpack_require__(/*! ./footer.component.html */ "./src/app/components/footer/footer.component.html"),
            styles: [__webpack_require__(/*! ./footer.component.css */ "./src/app/components/footer/footer.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], FooterComponent);
    return FooterComponent;
}());



/***/ }),

/***/ "./src/app/components/header/header.component.css":
/*!********************************************************!*\
  !*** ./src/app/components/header/header.component.css ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/* Shared */\n\n.loginBtn {\n    box-sizing: border-box;\n    position: relative;\n    /* width: 13em;  - apply for fixed size */\n    margin: 0.2em;\n    padding: 0 15px 0 46px;\n    border: none;\n    text-align: left;\n    line-height: 34px;\n    white-space: nowrap;\n    border-radius: 0.2em;\n    font-size: 16px;\n    color: #FFF;\n}\n\n.loginBtn:before {\n    content: \"\";\n    box-sizing: border-box;\n    position: absolute;\n    top: 0;\n    left: 0;\n    width: 34px;\n    height: 100%;\n}\n\n.loginBtn:focus {\n    outline: none;\n}\n\n.loginBtn:active {\n    box-shadow: inset 0 0 0 32px rgba(0, 0, 0, 0.1);\n}\n\n/* Facebook */\n\n.loginBtn--facebook {\n    background-color: #4C69BA;\n    background-image: linear-gradient(#4C69BA, #3B55A0);\n    /*font-family: \"Helvetica neue\", Helvetica Neue, Helvetica, Arial, sans-serif;*/\n    text-shadow: 0 -1px 0 #354C8C;\n}\n\n.loginBtn--facebook:before {\n    border-right: #364e92 1px solid;\n    background: url('https://s3-us-west-2.amazonaws.com/s.cdpn.io/14082/icon_facebook.png') 6px 6px no-repeat;\n}\n\n.loginBtn--facebook:hover,\n.loginBtn--facebook:focus {\n    background-color: #5B7BD5;\n    background-image: linear-gradient(#5B7BD5, #4864B1);\n}\n\n/* Google */\n\n.loginBtn--google {\n    /*font-family: \"Roboto\", Roboto, arial, sans-serif;*/\n    background: #DD4B39;\n}\n\n.loginBtn--google:before {\n    border-right: #BB3F30 1px solid;\n    background: url('https://s3-us-west-2.amazonaws.com/s.cdpn.io/14082/icon_google.png') 6px 6px no-repeat;\n}\n\n.loginBtn--google:hover,\n.loginBtn--google:focus {\n    background: #E74B37;\n}"

/***/ }),

/***/ "./src/app/components/header/header.component.html":
/*!*********************************************************!*\
  !*** ./src/app/components/header/header.component.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<header id=\"header\" id=\"home\" style=\"background-color: grey\">\n    <div class=\"container\">\n\n        <div class=\"row align-items-center justify-content-between d-flex\">\n            <div id=\"logo\">\n                <a href=\"#\"><img src=\"../../../assets/img/logo.png\" height=\"50\" width=\"200\" alt=\"\" title=\"\" /></a>\n            </div>\n\n\n            <nav id=\"nav-menu-container\">\n                <ul class=\"nav-menu\">\n                    <li class=\"menu-active\"><a href=\"#home\">My Idea</a></li>\n                    <li><a href=\"#videos\">Analysis guru</a></li>\n                    <li><a href=\"#speakers\">Flash mob</a></li>\n                    <li><a href=\"#schedule\">Coding challenge</a></li>\n                    <li><a href=\"#film\">Short film</a></li>\n                    <li><a href=\"#pubg\">PubG</a></li>\n                    <li><a href=\"#tiktok\">TikTok</a></li>\n                    <li *ngIf=\"!authService.loggedIn()\"><a [routerLink]=\"['/register']\" routerLinkActive=\"router-link-active\">Register</a></li>\n                    <li *ngIf=\"!authService.loggedIn()\">\n                        <a> <button type=\"button\" class=\"btn btn-primary\" data-toggle=\"modal\" data-target=\"#basicExampleModal\">\n                        Login\n                  </button></a>\n                    </li>\n\n                    <li *ngIf=\"authService.loggedIn()\"><a [routerLink]=\"['/events']\" routerLinkActive=\"router-link-active\">Events</a></li>\n\n\n\n                    <li *ngIf=\"authService.loggedIn()\">\n                        <a> <button type=\"button\" class=\" btn-secondary\" (click)=\"logout()\">LogOut</button>\n                        </a>\n                    </li>\n                </ul>\n            </nav>\n            <!-- #nav-menu-container -->\n        </div>\n\n    </div>\n\n\n\n    <!-- Button trigger modal -->\n    <!-- <button type=\"button\" class=\"btn btn-primary\" data-toggle=\"modal\" data-target=\"#basicExampleModal\">\n        Login\n  </button> -->\n\n    <!-- Modal -->\n    <div class=\"modal fade\" id=\"basicExampleModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel\" aria-hidden=\"true\">\n        <div class=\"modal-dialog\" role=\"document\">\n            <div class=\"modal-content\">\n                <div class=\"modal-header\">\n                    <h5 class=\"modal-title\" id=\"exampleModalLabel\">Safeathon Login</h5>\n                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n            <span aria-hidden=\"true\">&times;</span>\n          </button>\n                </div>\n                <!-- <div class=\"modal-body\"> -->\n                <p class=\"text-center\">\n                    Log in WIth <br>\n                    <!-- <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\" (click)=\"socialSignIn('google')\">Google</button> -->\n                    <button class=\"loginBtn loginBtn--google\" data-dismiss=\"modal\" (click)=\"socialSignIn('google')\">\n                        Login with Google\n                      </button>\n                    <!-- <button type=\"button\" class=\"btn btn-secondary\" (click)=\"socialSignIn('facebook')\">Facebook</button> -->\n                    <button class=\"loginBtn loginBtn--facebook\" (click)=\"socialSignIn('facebook')\">\n                        Login with Facebook\n                      </button>\n                </p>\n\n                <!-- </div> -->\n\n            </div>\n        </div>\n    </div>"

/***/ }),

/***/ "./src/app/components/header/header.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/components/header/header.component.ts ***!
  \*******************************************************/
/*! exports provided: HeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderComponent", function() { return HeaderComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/auth.service */ "./src/app/auth.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var angular_6_social_login__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! angular-6-social-login */ "./node_modules/angular-6-social-login/angular-6-social-login.umd.js");
/* harmony import */ var angular_6_social_login__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(angular_6_social_login__WEBPACK_IMPORTED_MODULE_3__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



// social login

var HeaderComponent = /** @class */ (function () {
    function HeaderComponent(authService, router, socialAuthService) {
        this.authService = authService;
        this.router = router;
        this.socialAuthService = socialAuthService;
    }
    HeaderComponent.prototype.ngOnInit = function () {
    };
    HeaderComponent.prototype.logout = function () {
        this.authService.deleteToken();
        this.router.navigateByUrl('/home');
    };
    HeaderComponent.prototype.socialSignIn = function (socialPlatform) {
        var _this = this;
        if (this.authService.isLoggedIn()) {
            alert('login');
            return this.router.navigateByUrl('/home');
        }
        var socialPlatformProvider;
        if (socialPlatform == 'facebook') {
            socialPlatformProvider = angular_6_social_login__WEBPACK_IMPORTED_MODULE_3__["FacebookLoginProvider"].PROVIDER_ID;
        }
        else if (socialPlatform == 'google') {
            socialPlatformProvider = angular_6_social_login__WEBPACK_IMPORTED_MODULE_3__["GoogleLoginProvider"].PROVIDER_ID;
        } // else if (socialPlatform == 'linkedin') {
        //   socialPlatformProvider = LinkedinLoginProvider.PROVIDER_ID;
        // }
        this.socialAuthService.signIn(socialPlatformProvider).then(function (userData) {
            // json({ "token": token });
            _this.email = userData['email'];
            console.log(userData['email']);
            var user = {
                email: userData['email'],
                password: ' '
            };
            _this.authService.login(user).subscribe(function (res) {
                console.log(res['token']);
                _this.authService.setToken(res['token']);
                window.location.reload();
                _this.router.navigate(['/home']);
            }, function (err) {
                _this.router.navigateByUrl('/register');
            });
            console.log(socialPlatform + ' sign in data : ', userData);
            // Now sign-in with userData
            // ...
        });
    };
    HeaderComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-header',
            template: __webpack_require__(/*! ./header.component.html */ "./src/app/components/header/header.component.html"),
            styles: [__webpack_require__(/*! ./header.component.css */ "./src/app/components/header/header.component.css")]
        }),
        __metadata("design:paramtypes", [src_app_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService1"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], angular_6_social_login__WEBPACK_IMPORTED_MODULE_3__["AuthService"]])
    ], HeaderComponent);
    return HeaderComponent;
}());



/***/ }),

/***/ "./src/app/components/home/home.component.css":
/*!****************************************************!*\
  !*** ./src/app/components/home/home.component.css ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/home/home.component.html":
/*!*****************************************************!*\
  !*** ./src/app/components/home/home.component.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- #header -->\n\n\n<!-- start banner Area -->\n<section class=\"banner-area relative\">\n    <div class=\"overlay overlay-bg\"></div>\n    <div class=\"container\">\n        <div class=\"row fullscreen d-flex align-items-center justify-content-center\">\n            <div class=\"banner-content col-lg-9 col-md-12\">\n                <h1 class=\"text-white\">\n                    SAFEATHON\n                </h1>\n                <div class=\"countdown\">\n                    <div id=\"timer\" class=\"text-white\"></div>\n                </div>\n                <h4><span class=\"lnr lnr-calendar-full\"></span> 14th January, 2019</h4>\n                <h4><span class=\"lnr lnr-map\"></span> T-HUB,IIIT-College</h4>\n            </div>\n        </div>\n    </div>\n</section>\n<!-- End banner Area -->\n\n<!-- Start home-video Area -->\n<section class=\"home-video-area\" id=\"home\">\n    <div class=\"container-fluid\">\n        <div class=\"row justify-content-end align-items-center\">\n            <div class=\"col-lg-4 no-padding about-right\">\n                <p class=\"top-title\">Imagination is beyond everything</p>\n                <h1> My Idea competition <br>\n                </h1>\n                <p><span> How to make your city the safest? </span></p>\n                <p>\n                    You would need to write an essay explaining How to make your city the safest.\n                    <br> a) A maximum of 30 bullet points or 700 words.\n                    <br> b) Alternatively, you may submit a video of 3 to 5 mins duration explaining your point.\n                    <br> c) Hardware,Software,Community based solutions are accepted.\n                    <br> d) Distinguished panel selects the top ideas and forwards idea to next level. A total of three levels happen.\n                    <!--\t<br> Registration link :<a href=\"https://docs.google.com/forms/d/1rdsYVFeqwWkFV5wLNJho1JduFmUvDD-oIV0xP10Ito0/edit?no_redirect=true\" \n              target=\"#\" class=\"heading\"><b>CLICK HERE</b></a>   -->\n                    <br> Registration link : <b> <a [routerLink]=\"['/events']\" routerLinkActive=\"router-link-active\" ><button type=\"button\" class=\"btn btn-success\">Register</button></a> </b>\n                </p>\n            </div>\n            <section class=\"video-area col-lg-6 no-padding\">\n                <div class=\"overlay overlay-bg\"></div>\n                <div class=\"containers\">\n                    <div class=\"video-content\">\n                        <img src=\"../../../assets/img/about3.jpg\" alt=\"\" height=\"300\" width=\"673\">\n                    </div>\n                </div>\n            </section>\n        </div>\n    </div>\n</section>\n<!-- End home-aboutus Area -->\n\n\n\n\n<!-- Start home-aboutus Area -->\n<section class=\"home-aboutus-area\" id=\"videos\">\n    <div class=\"container-fluid\">\n        <div class=\"row align-items-center\">\n            <div class=\"col-lg-6 no-padding about-left\">\n                <img class=\"img-fluid\" src=\"../../../assets/img/about2.jpg\" alt=\"\" height=\"300\" width=\"700\">\n            </div>\n            <div class=\"col-lg-4 no-padding about-right\">\n                <p class=\"top-title\">For the sharp minds</p>\n                <h1 class=\"text-white\">Analysis Guru<br>\n                </h1>\n                <p><span> Be a thinker, scrutinize and give your critique</span></p>\n                <p>\n                    We will shoot you an idea, you would need to understand it thoroughly and explain the pros,cons and suggestions to bolster the idea.\n                    <br> Level 1 : Competitor has to provide a critique on LifeOfGirl's concept (a startup working for women safety, details will be provided)\n                    <br> Level 2 : For the selected candidates from round one, a specific problem is put forward to solve/analyze and give a critique\n                    <br> Level 3 : The finalists would all be given a timed competition(online via Skype). The analysis has to be submitted in a maximum of 700 words, in a bullet point format.\n                    <!--\tRegistration link :<a href=\"https://docs.google.com/forms/d/1B4gdv773whlWhsenreSX-s37Z2Fy3mf7v2WApz0QaVA/edit\" \n              target=\"#\" class=\"heading\"><b>CLICK HERE</b></a> -->\n                    <br> Registration link : <b> <a [routerLink]=\"['/events']\" routerLinkActive=\"router-link-active\" ><button type=\"button\" class=\"btn btn-success\">Register</button></a> </b>\n                </p>\n            </div>\n        </div>\n    </div>\n</section>\n<!-- End home-aboutus Area -->\n\n\n<!-- Start home-video Area -->\n<section class=\"home-video-area\" id=\"speakers\">\n    <div class=\"container-fluid\">\n        <div class=\"row justify-content-end align-items-center\">\n            <div class=\"col-lg-4 no-padding about-right\">\n                <p class=\"top-title\">The competitions</p>\n                <h1>Flash Mob\n                </h1>\n                <p><span>Showcase your dance skills as a group</span></p>\n                <p>\n                    You would need to perform a flash mob in your own college for 4 songs: <br> a) Use songs from 4 different languages b) One song should be regarding girl child or women<br> c) The flash mob has to be recorded and submitted(youtube link)\n                    to Lifeofgirl team ( upload link will be provided)<br> d) Video format : i) Introduction ii) Dance video iii) Message.<br> Time\n                    < 4 mins <br> Size\n                        < 100 mb e) Winners are decided by distinguished judges based on popularity, impact, uniqueness and energy. <!-- <br> Registration link : <a href=\"https://docs.google.com/forms/d/1bzvcn4H2ZtGQKQ4j5uabO7YnAKlQQE7MC0-e2ksJues/edit\" target=\"#\" class=\"heading\"><b>CLICK HERE</b></a><br> -->\n                            <br> Registration link : <b> <a [routerLink]=\"['/events']\" routerLinkActive=\"router-link-active\" ><button type=\"button\" class=\"btn btn-success\">Register</button></a> </b>\n                </p>\n            </div>\n            <section class=\"video-area col-lg-6 no-padding\">\n                <div class=\"overlay overlay-bg\"></div>\n                <div class=\"containers\">\n                    <div class=\"video-content\">\n                        <img src=\"../../../assets/img/about1.jpg\" alt=\"\" height=\"300\" width=\"673\">\n                    </div>\n                </div>\n            </section>\n        </div>\n    </div>\n</section>\n<!-- End home-aboutus Area -->\n\n\n<!-- Start home-aboutus Area -->\n<section class=\"home-aboutus-area\" id=\"schedule\">\n    <div class=\"container-fluid\">\n        <div class=\"row align-items-center\">\n            <div class=\"col-lg-6 no-padding about-left\">\n                <img class=\"img-fluid\" src=\"../../../assets/img/coding.jpg\" alt=\"\" height=\"300\" width=\"700\">\n            </div>\n            <div class=\"col-lg-4 no-padding about-right\">\n                <p class=\"top-title\">Coders</p>\n                <h1 class=\"text-white\"> The coding challenge<br>\n                </h1>\n                <p><span>Showcase your coding skill</span></p>\n                <p>\n                    Let's put your coding skills to use, for a social project. Android/ IOS apps / webapps build for women security are eligible for the competition to enter into round one.\n                    <br> Criterion for selection: a) Innovation<br> b) Feasiblity<br> c) Ease of use<br> d) Scalability<br> First step :\n                    <!-- Registration\n                       <a href=\"https://docs.google.com/forms/d/1NYmbQzwcjcHMQ2Tg_xWN5fiVK6FMZNCn21U2Qh64cx0/edit\" \n                       target=\"#\" class=\"heading\"><b>CLICK HERE</b></a><br>  -->\n                    <br> Registration link : <b> <a [routerLink]=\"['/events']\" routerLinkActive=\"router-link-active\" ><button type=\"button\" class=\"btn btn-success\">Register</button></a> </b> Step two : Acceptance of terms and conditions <br> Step three\n                    : Apk & demo video submission Step four : Level 2 - Online Hackathon on given idea Step five : Final Hackathon in T-Hub,Hyderabad ( virtual participation is allowed)\n                </p>\n            </div>\n        </div>\n    </div>\n</section>\n<!-- End home-aboutus Area -->\n\n<!-- Start home-video Area -->\n<section class=\"home-video-area\" id=\"film\">\n    <div class=\"container-fluid\">\n        <div class=\"row justify-content-end align-items-center\">\n            <div class=\"col-lg-4 no-padding about-right\">\n                <p class=\"top-title\">Entertainment enthusiasts</p>\n                <h1> Are you crazy about movies <br>\n                </h1>\n                <p><span> Short film contest </span></p>\n                <p>\n                    Applicant can participate in two categories Category 1:\n                    <br> a) Women security\n                    <br> b) Women empowerment / Role of a woman\n                    <br> c) Misuse of woman-label by women\n                    <br> d) Love of parents / siblings /family Category 2: Advertisement /documentary / short film\n                    <br> a) Instant security and/or safe heaven concept of LifeOfGirl ( www.lifeofgirl.org ->safe heaven )\n                    <br> b) LifeOfGirl vision (www.lifeofgirl.org) Time :\n                    < 5mins Size : < 100 mb Upload link and other details : <b>will be updated soon </b>\n                        <!--\t<br> Registration link : <a href=\" https://docs.google.com/forms/d/1UZED9S6pSC4ukaC6JvLAUgsXSauoGfnsXrViVlMs4k4/edit\" \n                            target=\"#\" class=\"heading\"><b>CLICK HERE</b></a>   -->\n                        <br> Winners are decided by distinguished judges based on popularity, impact, uniqueness and energy.\n                        <br> Registration link : <b> <a [routerLink]=\"['/events']\" routerLinkActive=\"router-link-active\" ><button type=\"button\" class=\"btn btn-success\">Register</button></a> </b>\n                </p>\n            </div>\n            <section class=\"video-area col-lg-6 no-padding\">\n                <div class=\"overlay overlay-bg\"></div>\n                <div class=\"containers\">\n                    <div class=\"video-content\">\n                        <img src=\"../../../assets/img/shortfilm.jpg\" alt=\"\" height=\"300\" width=\"673\">\n                    </div>\n                </div>\n            </section>\n        </div>\n    </div>\n</section>\n<!-- End home-aboutus Area -->\n\n<section class=\"home-aboutus-area\" id=\"pubg\">\n    <div class=\"container-fluid\">\n        <div class=\"row align-items-center\">\n            <div class=\"col-lg-6 no-padding about-left\">\n                <img class=\"img-fluid\" src=\"../../../assets/img/pubg.jpg\" alt=\"\" height=\"300\" width=\"700\">\n            </div>\n            <div class=\"col-lg-4 no-padding about-right\">\n                <p class=\"top-title\">PUBG</p>\n                <h1 class=\"text-white\">The best chicken dinner<br>\n                </h1>\n                <p><span>Showcase your gaming skills</span></p>\n\n                <p>\n                    Let's put your gaming skills to use. Play PUBG and win against warriors all over India.\n                    <br> Process : Local games -> Top 2 Zonal games -> Top 2 Semi final -> Top 10 Finals -> Top 2 First step :\n                    <!-- Registration\n                       <a href=\"https://docs.google.com/forms/d/1JwunZYl1P-WLY0pGDnp6UiH9fseFhfZi6m9Md6i0C4M/edit\" \n                       target=\"#\" class=\"heading\"><b>CLICK HERE</b></a><br>   -->\n                    <br> Registration link : <b> <a [routerLink]=\"['/events']\" routerLinkActive=\"router-link-active\" ><button type=\"button\" class=\"btn btn-success\">Register</button></a> </b> Step two : Acceptance of terms and conditions <br> Step three\n                    : Play all level competition Step four : Win the finals and have chicken dinner\n                </p>\n            </div>\n        </div>\n    </div>\n</section>\n\n<section class=\"home-video-area\" id=\"tiktok\">\n    <div class=\"container-fluid\">\n        <div class=\"row justify-content-end align-items-center\">\n            <div class=\"col-lg-4 no-padding about-right\">\n                <p class=\"top-title\">Tik tok</p>\n                <h1> are you a specialist<br>\n                </h1>\n                <p><span> Tik tok contest </span></p>\n                <p>\n                    Tik Tok competition :\n                    <br> Showcase your TikTok skills to the nation. Be a TikTok winner.\n                    <br> 1) Level 1 submission :Make a video which is not obscene and submit to us.\n                    <br> 2) Level 2 : Level 1 winners provide a second tik tok video which is posted on safeathon page and website. The most popular videos go the next round.\n                    <br> 3) Level 3 : Level 2 winners are given topic for tik tok video and LifeOfGirl hosts the video for likes & comments.\n                    <br> 4) Top 2 TikTokers are crowned, rewarded and published by Safeathon & LifeOfGirl.\n                    <!--<br> Registration link : <a href=\" https://docs.google.com/forms/d/1UZED9S6pSC4ukaC6JvLAUgsXSauoGfnsXrViVlMs4k4/edit\" \n                            target=\"#\" class=\"heading\"><b>CLICK HERE</b></a> -->\n                    <br> Registration link : <b> <a [routerLink]=\"['/events']\" routerLinkActive=\"router-link-active\" ><button type=\"button\" class=\"btn btn-success\">Register</button></a> </b>\n                </p>\n            </div>\n            <section class=\"video-area col-lg-6 no-padding\">\n                <div class=\"overlay overlay-bg\"></div>\n                <div class=\"containers\">\n                    <div class=\"video-content\">\n                        <img src=\"../../../assets/img/tiktok.jpg\" alt=\"\" height=\"300\" width=\"673\">\n                    </div>\n                </div>\n            </section>\n        </div>\n    </div>\n</section>\n\n<!-- Start home-video Area -->\n<section class=\"home-video-area\">\n    <div class=\"container-fluid bgcolor\">\n        <div>\n            <div>\n\n                <br><br>\n                <h1 class=\"whyJoin\"> Why join us</h1><br>\n                <div class=\"texts\"><br>\n                    <p>\n                        <h4>\n                            <div class=\"heading\">\n                                Network :</div>\n                        </h4>\n                        Thousands of enthusiastic students from all over the world participate in Safeathon 1.0. You can network with each of them using the networking tool.<br><br>\n\n                        <h4>\n                            <div class=\"heading\">\n                                Recognition :</div>\n                        </h4>\n\n                        Along with the awards & rewards, the winners are specially advertised in all LOG channels.<br><br>\n\n                        <h4>\n                            <div class=\"heading\">\n                                Role in LOG:</div>\n                        </h4>\n                        The best of the participants in various fields would be choosen and given an opportunity to work with LOG core team.\n                </div>\n                <br><br>\n            </div>\n        </div>\n    </div>\n</section>\n\n<!-- End home-aboutus Area -->\n\n\n\n\n<!-- Start spekers Area -->\n<section class=\"spekers-area pb-100\" id=\"speakers\">\n    <div class=\"container-fluid\">\n        <div class=\"row no-padding\">\n            <div class=\"active-speaker-carusel col-lg-12 no-padding\">\n                <div class=\"single-speaker item\">\n                    <div class=\"container\">\n                        <div class=\"row align-items-center\">\n                            <div class=\"col-md-6 speaker-img no-padding\">\n                                <img src=\"../../../assets/img/s1.jpg\" alt=\"\">\n                            </div>\n                            <div class=\"col-md-6 speaker-info no-padding\">\n                                <h6 class=\"text-uppercase\">JUDGE 01</h6>\n                                <h1 class=\"text-white\">published soon</h1>\n                                <p>\n                                    He/She is an experienced campaigner in the dancing arena. He/She has acted as Judge for similar contests many a time.\n                                </p>\n                                <p><span class=\"lnr lnr-phone-handset\"></span>to be updated</p>\n                                <p><span class=\"lnr lnr-location\"></span>to be updated</p>\n                                <ul>\n                                    <a href=\"#\"><i class=\"fa fa-facebook\"></i></a>\n                                    <a href=\"#\"><i class=\"fa fa-twitter\"></i></a>\n                                    <a href=\"#\"><i class=\"fa fa-dribbble\"></i></a>\n                                    <a href=\"#\"><i class=\"fa fa-behance\"></i></a>\n                                </ul>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n                <div class=\"single-speaker item\">\n                    <div class=\"container\">\n                        <div class=\"row align-items-center\">\n                            <div class=\"col-md-6 speaker-img no-padding\">\n                                <img src=\"../../../assets/img/s2.jpg\" alt=\"\">\n                            </div>\n                            <div class=\"col-md-6 speaker-info no-padding\">\n                                <h6 class=\"text-uppercase\">JUDGE 02</h6>\n                                <h1 class=\"text-white\">Published soon</h1>\n                                <p>\n                                    He/She is an experienced campaigner in the software arena. He/She has been serving the software industry since a long time.\n\n                                </p>\n                                <p><span class=\"lnr lnr-phone-handset\"></span>to be updated</p>\n                                <p><span class=\"lnr lnr-location\"></span>to be updated</p>\n                                <ul>\n                                    <a href=\"#\"><i class=\"fa fa-facebook\"></i></a>\n                                    <a href=\"#\"><i class=\"fa fa-twitter\"></i></a>\n                                    <a href=\"#\"><i class=\"fa fa-dribbble\"></i></a>\n                                    <a href=\"#\"><i class=\"fa fa-behance\"></i></a>\n                                </ul>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n                <div class=\"single-speaker item\">\n                    <div class=\"container\">\n                        <div class=\"row align-items-center\">\n                            <div class=\"col-md-6 speaker-img no-padding\">\n                                <img src=\"../../../assets/img/s1.jpg\" alt=\"\">\n                            </div>\n                            <div class=\"col-md-6 speaker-info no-padding\">\n                                <h6 class=\"text-uppercase\">JUDGE 03</h6>\n                                <h1 class=\"text-white\">published soon</h1>\n                                <p>\n                                    He/She is an experienced campaigner in the dancing arena. He/She has acted as Judge for similar contests many a time.\n                                </p>\n                                <p><span class=\"lnr lnr-phone-handset\"></span>to be updated</p>\n                                <p><span class=\"lnr lnr-location\"></span>to be updated</p>\n                                <ul>\n                                    <a href=\"#\"><i class=\"fa fa-facebook\"></i></a>\n                                    <a href=\"#\"><i class=\"fa fa-twitter\"></i></a>\n                                    <a href=\"#\"><i class=\"fa fa-dribbble\"></i></a>\n                                    <a href=\"#\"><i class=\"fa fa-behance\"></i></a>\n                                </ul>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n                <div class=\"single-speaker item\">\n                    <div class=\"container\">\n                        <div class=\"row align-items-center\">\n                            <div class=\"col-md-6 speaker-img no-padding\">\n                                <img src=\"../../../assets/img/s2.jpg\" alt=\"\">\n                            </div>\n                            <div class=\"col-md-6 speaker-info no-padding\">\n                                <h6 class=\"text-uppercase\">JUDGE 04</h6>\n                                <h1 class=\"text-white\">published soon</h1>\n                                <p>\n                                    He/She is an experienced campaigner in the movie industry. He/She has acted as Judge for similar contests many a time.\n                                </p>\n                                <p><span class=\"lnr lnr-phone-handset\"></span>to be updated</p>\n                                <p><span class=\"lnr lnr-location\"></span>to be updated</p>\n                                <ul>\n                                    <a href=\"#\"><i class=\"fa fa-facebook\"></i></a>\n                                    <a href=\"#\"><i class=\"fa fa-twitter\"></i></a>\n                                    <a href=\"#\"><i class=\"fa fa-dribbble\"></i></a>\n                                    <a href=\"#\"><i class=\"fa fa-behance\"></i></a>\n                                </ul>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</section>\n<!-- End spekers Area -->\n\n\n<!-- Start schedule Area -->\n<!-- <section class=\"schedule-area pb-100\" id=\"schedule\">\n      <div class=\"container\">\n        <div class=\"row d-flex justify-content-center\">\n          <div class=\"menu-content pb-70 col-lg-8\">\n            <div class=\"title text-center\">\n              <h1 class=\"mb-10\">How Our Customers Treat Us</h1>\n              <p>Who are in extremely love with eco friendly system.</p>\n            </div>\n          </div>\n        </div>\t\t\t\t\t\t\n        <div class=\"row\">\n          <div class=\"table-wrap col-lg-12\">\n            <table class=\"schdule-table table table-bordered\">\n                <thead class=\"thead-light\">\n                  <tr>\n                    <th class=\"head\" scope=\"col\">sl</th>\n                    <th class=\"head\" scope=\"col\">session</th>\n                    <th class=\"head\" scope=\"col\">speaker</th>\n                    <th class=\"head\" scope=\"col\">vanue</th>\n                    <th class=\"head\" scope=\"col\">time</th>\n                  </tr>\n                </thead>\n                <tbody>\n                  <tr>\n                    <th class=\"name\" scope=\"row\">01</th>\n                    <td>Reception & Taling Seats</td>\t\t\t\t      \n                    <td>Isabelle Cooper</td>\t\t\t\t      \n                    <td>3rd Hall Room</td>\t\t\t\t      \n                    <td>02.00</td>\t\t\t\t\t      \n                  </tr>\n                  <tr>\n                    <th class=\"name\" scope=\"row\">02</th>\n                    <td>Breakfast and rest</td>\t\t\t\t      \n                    <td>N/A</td>\t\t\t\t      \n                    <td>4th Hall Room</td>\t\t\t\t      \n                    <td>02.00</td>\t\t\t\t\t\t      \n                  </tr>\n                  <tr>\n                    <th class=\"name\" scope=\"row\">03</th>\n                    <td>Reception & Taling Seats</td>\t\t\t\t      \n                    <td>Jane Daniel</td>\t\t\t\t      \n                    <td>2nd Hall Room</td>\t\t\t\t      \n                    <td>02.00</td>\t\t\t\t\t\t      \n                  </tr>\n                  <tr>\n                    <th class=\"name\" scope=\"row\">04</th>\n                    <td>Next generation speech</td>\t\t\t\t      \n                    <td>Billy Barton</td>\t\t\t\t      \n                    <td>1st Hall Room</td>\t\t\t\t      \n                    <td>02.00</td>\t\t\t\t\t\t      \n                  </tr>\n                  <tr>\n                    <th class=\"name\" scope=\"row\">05</th>\n                    <td>Sppech for young people</td>\t\t\t\t      \n                    <td>Flora Gonzales</td>\t\t\t\t      \n                    <td>4rd Hall Room</td>\t\t\t\t      \n                    <td>02.00</td>\t\t\t\t\t      \n                  </tr>\n                  <tr>\n                    <th class=\"name\" scope=\"row\">06</th>\n                    <td>Lunch Break</td>\t\t\t\t      \n                    <td>N/A</td>\t\t\t\t      \n                    <td>3rd Hall Room</td>\t\t\t\t      \n                    <td>02.00</td>\t\t\t\t\t      \n                  </tr>\n                  <tr>\n                    <th class=\"name\" scope=\"row\">07</th>\n                    <td>Sppech for Middle aged people</td>\t\t\t\t \n                    <td>Francisco Barrett</td>\t\t\t\t      \n                    <td>1st Hall Room</td>\t\t\t\t      \n                    <td>02.00</td>\t\t\t\t\t      \n                  </tr>\t\t\t\t\t\t\t\t\t\t\t    \n                </tbody>\n            </table>\t\t\t\t\t\t\t\n          </div>\n        </div>\n      </div>\t\n    </section> -->\n<!-- End schedule Area -->"

/***/ }),

/***/ "./src/app/components/home/home.component.ts":
/*!***************************************************!*\
  !*** ./src/app/components/home/home.component.ts ***!
  \***************************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var HomeComponent = /** @class */ (function () {
    function HomeComponent() {
    }
    HomeComponent.prototype.ngOnInit = function () {
    };
    HomeComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-home',
            template: __webpack_require__(/*! ./home.component.html */ "./src/app/components/home/home.component.html"),
            styles: [__webpack_require__(/*! ./home.component.css */ "./src/app/components/home/home.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], HomeComponent);
    return HomeComponent;
}());



/***/ }),

/***/ "./src/app/components/login/login.component.css":
/*!******************************************************!*\
  !*** ./src/app/components/login/login.component.css ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/login/login.component.html":
/*!*******************************************************!*\
  !*** ./src/app/components/login/login.component.html ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <form (submit)=\"twitterlogin()\">\n    <input type=\"submit\" class=\"btn btn-primary\" value=\"twitter\">\n</form> -->\n<!-- <a href=\"https://localhost:3000/auth/facebook\">fb</a>\n<br>-->\n<!-- <a href=\"http://localhost:3000/auth/twitter\">twitter</a> -->\n\n\n<h1>\n    Sign in\n</h1>\n\n<button (click)=\"socialSignIn('facebook')\">Sign in with Facebook</button>\n<button (click)=\"socialSignIn('google')\">Sign in with Google</button>"

/***/ }),

/***/ "./src/app/components/login/login.component.ts":
/*!*****************************************************!*\
  !*** ./src/app/components/login/login.component.ts ***!
  \*****************************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/auth.service */ "./src/app/auth.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var angular_6_social_login__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! angular-6-social-login */ "./node_modules/angular-6-social-login/angular-6-social-login.umd.js");
/* harmony import */ var angular_6_social_login__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(angular_6_social_login__WEBPACK_IMPORTED_MODULE_3__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



// social login

var LoginComponent = /** @class */ (function () {
    function LoginComponent(authService, router, socialAuthService) {
        this.authService = authService;
        this.router = router;
        this.socialAuthService = socialAuthService;
    }
    LoginComponent.prototype.ngOnInit = function () {
        if (this.authService.isLoggedIn()) {
            alert('login');
            return this.router.navigateByUrl('/home');
        }
    };
    LoginComponent.prototype.socialSignIn = function (socialPlatform) {
        var _this = this;
        if (this.authService.isLoggedIn()) {
            alert('login');
            return this.router.navigateByUrl('/home');
        }
        var socialPlatformProvider;
        if (socialPlatform == 'facebook') {
            socialPlatformProvider = angular_6_social_login__WEBPACK_IMPORTED_MODULE_3__["FacebookLoginProvider"].PROVIDER_ID;
        }
        else if (socialPlatform == 'google') {
            socialPlatformProvider = angular_6_social_login__WEBPACK_IMPORTED_MODULE_3__["GoogleLoginProvider"].PROVIDER_ID;
        } // else if (socialPlatform == 'linkedin') {
        //   socialPlatformProvider = LinkedinLoginProvider.PROVIDER_ID;
        // }
        this.socialAuthService.signIn(socialPlatformProvider).then(function (userData) {
            // json({ "token": token });
            _this.email = userData['email'];
            console.log(userData['email']);
            var user = {
                email: userData['email'],
                password: ' '
            };
            _this.authService.login(user).subscribe(function (res) {
                console.log(res['token']);
                _this.authService.setToken('cool:-------------' + res['token']);
                _this.router.navigateByUrl('/home');
            }, function (err) {
                _this.router.navigate(['/register/' + _this.email]);
            });
            console.log(socialPlatform + ' sign in data : ', userData);
            // Now sign-in with userData
            // ...
        });
    };
    LoginComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! ./login.component.html */ "./src/app/components/login/login.component.html"),
            styles: [__webpack_require__(/*! ./login.component.css */ "./src/app/components/login/login.component.css")]
        }),
        __metadata("design:paramtypes", [src_app_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService1"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], angular_6_social_login__WEBPACK_IMPORTED_MODULE_3__["AuthService"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/components/register/register.component.css":
/*!************************************************************!*\
  !*** ./src/app/components/register/register.component.css ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".center-div {\n    margin: 0 auto;\n    max-width: 500px;\n    /* height: 100px; */\n    /* background-color: #ccc; */\n    border-radius: 3px;\n}\n\n.example-container {\n    display: flex;\n    flex-direction: column;\n}\n\n.example-container>* {\n    width: 100%;\n}\n\n.mat-form-field {\n    display: block;\n    position: relative;\n    text-align: left;\n}"

/***/ }),

/***/ "./src/app/components/register/register.component.html":
/*!*************************************************************!*\
  !*** ./src/app/components/register/register.component.html ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<br>\n<h2 class=\"text-center\">\n    SAFEATHON REGISTRATION\n</h2>\n<br>\n<form [formGroup]=\"registerForm\" (ngSubmit)=\"onSubmit()\">\n    <div class=\"container\">\n\n\n        <div class=\"row align-items-center\">\n            <div class=\"col\">\n                <mat-form-field appearance=\"outline\">\n                    <mat-label> Name</mat-label>\n                    <input matInput placeholder=\"Your Name\" formControlName=\"Name\" [ngClass]=\"{ 'is-invalid': submitted && f.Name.errors }\">\n                    <mat-icon matSuffix>account_circle</mat-icon>\n                    <!-- <mat-hint>eg.chilakuri karthikeya</mat-hint> -->\n\n                </mat-form-field>\n\n            </div>\n\n            <div class=\"col\">\n                <mat-form-field appearance=\"outline\" aria-disabled=\"true\">\n                    <mat-label> Email</mat-label>\n                    <input matInput placeholder=\"Your Email\" disabled formControlName=\"email\" [ngClass]=\"{ 'is-invalid': submitted && f.email.errors }\">\n                    <mat-icon matSuffix>alternate_email</mat-icon>\n                    <!-- <mat-hint>eg.lifeofgirl@gmail.com</mat-hint> -->\n                </mat-form-field>\n            </div>\n            <div class=\"col\">\n                <mat-form-field appearance=\"outline\">\n                    <mat-label> Ph No</mat-label>\n                    <input matInput placeholder=\"Your Ph_no\" formControlName=\"ph_no\" [ngClass]=\"{ 'is-invalid': submitted && f.ph_no.errors }\">\n                    <mat-icon matSuffix>call</mat-icon>\n                    <!-- <mat-hint>9000101439</mat-hint> -->\n                </mat-form-field>\n            </div>\n\n        </div>\n        <div class=\"row align-items-center\">\n            <div class=\"col\">\n                <mat-form-field>\n                    <select matNativeControl required name=\"country\" class=\"countries\" id=\"countryId\" formControlName=\"country\">\n                    <option value=\"\">Select Country</option>\n                  </select>\n                </mat-form-field>\n            </div>\n            <div class=\"col\">\n                <mat-form-field>\n                    <select matNativeControl required name=\"state\" class=\"states\" id=\"stateId\" formControlName=\"state\">\n                  <option value=\"\">Select State</option>\n                </select>\n                </mat-form-field>\n            </div>\n            <div class=\"col\">\n                <mat-form-field>\n                    <select matNativeControl required name=\"city\" class=\"cities\" id=\"cityId\" formControlName=\"city\">\n                <option value=\"\">Select City</option>\n              </select>\n                </mat-form-field>\n            </div>\n        </div>\n\n        <div class=\"row align-items-center\">\n            <div class=\"col\">\n            </div>\n\n            <div class=\"col\">\n                <mat-form-field appearance=\"outline\">\n                    <mat-label>College/Organization</mat-label>\n                    <input matInput placeholder=\"College/Organization\" formControlName=\"college\" [ngClass]=\"{ 'is-invalid': submitted && f.college.errors }\">\n                    <!-- <mat-icon matSuffix>alternate_email</mat-icon> -->\n                    <!-- <mat-hint>College/Organization</mat-hint> -->\n                </mat-form-field>\n            </div>\n\n            <div class=\"col\">\n            </div>\n        </div>\n\n        <p class=\"text-center\">\n            <button mat-stroked-button color=\"primary\">Register</button>\n        </p>\n    </div>\n</form>"

/***/ }),

/***/ "./src/app/components/register/register.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/components/register/register.component.ts ***!
  \***********************************************************/
/*! exports provided: RegisterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterComponent", function() { return RegisterComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/auth.service */ "./src/app/auth.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




// import { ActivatedRoute,  Routes } from '@angular/router';
var RegisterComponent = /** @class */ (function () {
    function RegisterComponent(authService, router, formBuilder, route) {
        this.authService = authService;
        this.router = router;
        this.formBuilder = formBuilder;
        this.route = route;
        this.submitted = false;
        this.model = {
        // email: this.route.snapshot.paramMap.get('id')
        };
    }
    RegisterComponent.prototype.ngOnInit = function () {
        // const id = this.route.snapshot.paramMap.get('id');
        if (this.authService.isLoggedIn()) {
            // alert('login');
            return this.router.navigateByUrl('/home');
        }
        console.log(this.route.snapshot.paramMap.get('id'));
        this.registerForm = this.formBuilder.group({
            Name: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            email: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].email]],
            ph_no: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].minLength(10)]],
            country: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]],
            state: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            city: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            college: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]
        });
    };
    Object.defineProperty(RegisterComponent.prototype, "f", {
        get: function () { return this.registerForm.controls; },
        enumerable: true,
        configurable: true
    });
    RegisterComponent.prototype.onSubmit = function () {
        var _this = this;
        this.submitted = true;
        // stop here if form is invalid
        if (this.registerForm.invalid) {
            console.log('failure');
            // alert('d');
            return;
        }
        this.authService.register(this.registerForm.value).subscribe(function (res) {
            alert('Registred successfully');
            _this.router.navigateByUrl('/home');
        }, function (err) {
            if (err.status === 422) {
                alert('Email already exists');
            }
            else {
                console.log('err');
            }
        });
        console.log(this.registerForm.value);
    };
    RegisterComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-register',
            template: __webpack_require__(/*! ./register.component.html */ "./src/app/components/register/register.component.html"),
            styles: [__webpack_require__(/*! ./register.component.css */ "./src/app/components/register/register.component.css")]
        }),
        __metadata("design:paramtypes", [src_app_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService1"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]])
    ], RegisterComponent);
    return RegisterComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false,
    apiBaseUrl: 'https://tranquil-ridge-81301.herokuapp.com/users'
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! hammerjs */ "./node_modules/hammerjs/hammer.js");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");





if (_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_3__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /home/jyoshna/Desktop/dec/safeathon/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map