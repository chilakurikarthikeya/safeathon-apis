const express = require('express');
const router = express.Router();
const User = require('../models/user');
const passport = require('passport');


const mongoose = require('mongoose');
const _ = require('lodash');
const new_user = mongoose.model('User')
mongoose.set('useFindAndModify', false);

require('../passport/passport');
// require('../models/user');


//register
router.post('/register', (req, res, next) => {
    let newUser = new User({
        name: req.body.Name,
        email: req.body.email,
        ph_no: req.body.ph_no,
        country: req.body.country,
        state: req.body.state,
        city: req.body.city,
        college: req.body.college,

    });
    console.log(newUser);
    User.addUser(newUser, (err, user) => {
        if (err) {
            // console.log('error');
            if (err.code == 11000)
                res.status(422).send(['Duplicate email adrress found.']);
            else
                res.json({ successs: false, msg: "faild to register" });
        } else {
            console.log('success');
            res.json({ successs: true, msg: 'User registred' });
        }
    });
});

// getting eamil after login for regpage
router.post('/get_usemail', (req, res, next) => {

})



// jwt

router.post('/authenticate', (req, res, next) => {
    // res.send('called');
    //call fro passport authentication
    passport.authenticate('local', (err, user, info) => {
        //error from passport middleware
        if (err) return res.status(400).json(err);
        // error from passport middleware
        else if (user) {
            // console.log(json({ "token": user.generateJwt() }));
            return res.status(200).json({ "token": user.generateJwt() });
        }
        // unknown user or wrong password
        else return res.status(404).json(info);
    })(req, res);
});


const jwtHelper = require('../config/jwtHelper');
// const User = mongoose.model('Rep')

router.get('/user_profile', jwtHelper.verifyJwtToken, (req, res, next) => {
    console.log('called userp');
    User.findOne({ _id: req._id },
        (err, user) => {
            if (!user) {
                console.log("no usedr");
                return res.status(404).json({ status: false, message: 'User record not found.' });

            } else {

                console.log(" user found");
                return res.status(200).json({ status: true, user: user });
            }
        }
    );
});

//updating events

router.put('/update', jwtHelper.verifyJwtToken, (req, res, next) => {
    User.findById({ _id: req._id },
        (err, user) => {
            console.log('called');
            console.log(req._id);
            if (err) {
                return json({ status: false, message: 'record not updated.' });
            } else {
                // MyIdea
                if (req.body.event == 1) {
                    if (req.body.opt == 1) {
                        console.log('called1');
                        user.events.MyIdea_reg = true;
                        user.save((err, user) => {
                            if (err)
                                return res.status(404).json({ status: false, message: 'record not updated.' });

                            return res.status(202).json({ status: true, message: 'record updated1.' });

                        });
                    } else {
                        console.log('called2');
                        user.events.MyIdea_payment = true;
                        user.save((err, user) => {
                            if (err)
                                return res.status(404).json({ status: false, message: 'record not updated.' });
                            else
                                return res.status(202).json({ status: true, message: 'record updated2.' });

                        });
                    }
                }

                //AnalysisGuru
                if (req.body.event == 2) {
                    if (req.body.opt == 1) {
                        console.log('called1');
                        user.events.AnalysisGuru_reg = true;
                        user.save((err, user) => {
                            if (err)
                                return res.status(404).json({ status: false, message: 'record not updated.' });

                            return res.status(202).json({ status: true, message: user });

                        });
                    } else {
                        console.log('called2');
                        user.events.AnalysisGuru_payment = true;
                        user.save((err, user) => {
                            if (err)
                                return res.status(404).json({ status: false, message: 'record not updated.' });
                            else
                                return res.status(202).json({ status: true, message: 'record updated2.' });

                        });
                    }
                }

                // FlashMod
                if (req.body.event == 3) {
                    if (req.body.opt == 1) {
                        console.log('called1');
                        user.events.FlashMob_reg = true;
                        user.save((err, user) => {
                            if (err)
                                return res.status(404).json({ status: false, message: 'record not updated.' });

                            return res.status(202).json({ status: true, message: 'record updated1.' });

                        });
                    } else {
                        console.log('called2');
                        user.events.FlashMob_payment = true;
                        user.save((err, user) => {
                            if (err)
                                return res.status(404).json({ status: false, message: 'record not updated.' });
                            else
                                return res.status(202).json({ status: true, message: 'record updated2.' });

                        });
                    }
                }

                // coddingChallange
                if (req.body.event == 4) {
                    if (req.body.opt == 1) {
                        console.log('called1');
                        user.events.CoadingChallenge_reg = true;
                        user.save((err, user) => {
                            if (err)
                                return res.status(404).json({ status: false, message: 'record not updated.' });

                            return res.status(202).json({ status: true, message: 'record updated1.' });

                        });
                    } else {
                        console.log('called2');
                        user.events.CoadingChallenge_payment = true;
                        user.save((err, user) => {
                            if (err)
                                return res.status(404).json({ status: false, message: 'record not updated.' });
                            else
                                return res.status(202).json({ status: true, message: 'record updated2.' });

                        });
                    }
                }

                // shortFilim
                if (req.body.event == 5) {
                    if (req.body.opt == 1) {
                        console.log('called1');
                        user.events.ShortFilm_reg = true;
                        user.save((err, user) => {
                            if (err)
                                return res.status(404).json({ status: false, message: 'record not updated.' });

                            return res.status(202).json({ status: true, message: 'record updated1.' });

                        });
                    } else {
                        console.log('called2');
                        user.events.ShortFilm_payment = true;
                        user.save((err, user) => {
                            if (err)
                                return res.status(404).json({ status: false, message: 'record not updated.' });
                            else
                                return res.status(202).json({ status: true, message: 'record updated2.' });

                        });
                    }
                }

                // pubg
                if (req.body.event == 6) {
                    if (req.body.opt == 1) {
                        console.log('called1');
                        user.events.PubG_reg = true;
                        user.save((err, user) => {
                            if (err)
                                return res.status(404).json({ status: false, message: 'record not updated.' });

                            return res.status(202).json({ status: true, message: 'record updated1.' });

                        });
                    } else {
                        console.log('called2');
                        user.events.PubG_payment = true;
                        user.save((err, user) => {
                            if (err)
                                return res.status(404).json({ status: false, message: 'record not updated.' });
                            else
                                return res.status(202).json({ status: true, message: 'record updated2.' });

                        });
                    }
                }

                // TikTok
                if (req.body.event == 7) {
                    if (req.body.opt == 1) {
                        console.log('called1');
                        user.events.TikTok_reg = true;
                        user.save((err, user) => {
                            if (err)
                                return res.status(404).json({ status: false, message: 'record not updated.' });

                            return res.status(202).json({ status: true, message: 'record updated1.' });

                        });
                    } else {
                        console.log('called2');
                        user.events.TikTok_payment = true;
                        user.save((err, user) => {
                            if (err)
                                return res.status(404).json({ status: false, message: 'record not updated.' });
                            else
                                return res.status(202).json({ status: true, message: 'record updated2.' });

                        });
                    }
                }

            }
        });
});




router.get('/twitter/:token', (res, req) => {
    res.redirect('/register')
})

module.exports = router;