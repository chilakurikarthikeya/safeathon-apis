const express = require('express');

const path = require('path');
const bodyParser = require('body-parser');
const cors = require('cors');
require('./passport/passport');
const passport = require('passport');
const mongoose = require('mongoose');
const config = require('./config/database');



const app = express();

// var social = require('./passport/passport')(app, passport);

//connect to db
mongoose.connect(config.database);

//on connect 
mongoose.connection.on('connected', () => {
    console.log('connected to db');
});
//on error
mongoose.connection.on('error', (err) => {
    console.log('Database error' + err);
});



const users = require('./routes/users')
    // port number 
    // const port = 3000;
const port = process.env.PORT || 8080;


// set static folder
app.use(express.static(path.join(__dirname, 'public')))

// CORS Middleware
app.use(cors());

//Body Parser Middleware
app.use(bodyParser.json());

//passport  middleware
// require('./passport/passport'); declread above
app.use(passport.initialize());

app.use('/users', users);



//index route
app.get('/', (req, res) => {
    res.send('invalid endpoint');
})



//start server
app.listen(port, () => {
    console.log('server satarted on' + port);
});