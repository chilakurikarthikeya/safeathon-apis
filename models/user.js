const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const config = require('../config/database');

const jwt = require('jsonwebtoken');
mongoose.set('useCreateIndex', true);
// User Schema
const UserSchema = mongoose.Schema({
    name: {
        type: String
    },
    email: {
        type: String,
        required: 'Email can\'t be empty',
        unique: true
    },
    ph_no: {
        type: String,
        // required: true
    },
    country: {
        type: String,
        // required: true
    },
    state: {
        type: String,
        // required: true
    },
    city: {
        type: String,
        // required: true
    },
    college: {
        type: String,
    },
    events: {
        MyIdea_reg: Boolean,
        MyIdea_payment: Boolean,

        AnalysisGuru_reg: Boolean,
        AnalysisGuru_payment: Boolean,

        FlashMob_reg: Boolean,
        FLashMob_payment: Boolean,

        CoadingChallenge_reg: Boolean,
        CoadingChallenge_payment: Boolean,

        ShortFilm_reg: Boolean,
        ShortFilm_payment: Boolean,

        AnalysisGuru_reg: Boolean,
        AnalysisGuru_payment: Boolean,

        PubG_reg: Boolean,
        PubG_payment: Boolean,

        TikTok_reg: Boolean,
        TikTok_payment: Boolean

    }





});



// module.exports.getUserById = function(id, callback){
//     User.findById(id, callback);
//   }







module.exports.getUserByemail = function(email, callback) {
    const query = { email: email }
    User.findOne(query, callback);
}




//jwt
JWT_SECRET = 'log2';
JWT_EXP = '10d'
    // JWT_EXP

// UserSchema.methods.generateJwt = function() {
//     return jwt.sign({ _id: this._id },
//         JWT_SECRET);

// }

UserSchema.methods.generateJwt = function() {

    // console.log(jwt.sign({ _id: this._id },
    //     JWT_SECRET, {
    //         expiresIn: JWT_EXP
    //     }));
    return jwt.sign({ _id: this._id },
        JWT_SECRET, {
            expiresIn: JWT_EXP
        });
}


const User = module.exports = mongoose.model('User', UserSchema);

module.exports.addUser = function(newUser, callback) {
    // if (err) throw err;
    console.log('called');

    newUser.events.MyIdea_reg = false;
    newUser.events.MyIdea_payment = false;

    newUser.events.AnalysisGuru_reg = false;
    newUser.events.AnalysisGuru_payment = false;

    newUser.events.FlashMob_reg = false;
    newUser.events.FLashMob_payment = false;

    newUser.events.CoadingChallenge_reg = false
    newUser.events.CoadingChallenge_payment = false;

    newUser.events.ShortFilm_reg = false;
    newUser.events.ShortFilm_payment = false;

    newUser.events.PubG_reg = false;
    newUser.events.PubG_payment = false;

    newUser.events.TikTok_reg = false;
    newUser.events.TikTok_payment = false;

    newUser.save(callback);

}